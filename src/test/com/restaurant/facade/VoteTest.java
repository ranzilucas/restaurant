package com.restaurant.facade;

import com.restaurant.dao.EmployeeDAO;
import com.restaurant.dao.RestaurantDAO;
import com.restaurant.model.Employee;
import com.restaurant.model.Restaurant;
import org.junit.Test;

/**
 * Created by lucas on 21/07/14.
 */
public class VoteTest {
    @Test
    public void vote() {
        VoteFacade voteFacade = new VoteFacade();

        Restaurant restaurant = new Restaurant();
        restaurant.setName("teste");
        restaurant.setActive(true);

        RestaurantDAO restaurantDAO = new RestaurantDAO();
        restaurantDAO.save(restaurant);

        Employee employee = new Employee();
        employee.setName("Teste");
        employee.setVote(true);

        EmployeeDAO employeeDAO = new EmployeeDAO();
        employeeDAO.save(employee);

        String s = voteFacade.vote(Integer.toString(restaurant.getId()), Integer.toString(employee.getId()));

        assert s.equals("Sucesso ao votar");

        employeeDAO.remove(employee);
        restaurantDAO.remove(restaurant);

    }
}
