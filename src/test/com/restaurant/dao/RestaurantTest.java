package com.restaurant.dao;

import com.restaurant.model.Restaurant;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by lucas on 16/07/14.
 */
public class RestaurantTest {

    @Test
    public void insert(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistencePU");
        EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();

            Restaurant restaurant = new Restaurant();
            restaurant.setName("abcd");

            em.persist(restaurant);

            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            emf.close();
        }

    }


}