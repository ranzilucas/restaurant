var myApp = angular.module('myApp', ['ngRoute']);

myApp.factory('server', function ($http) {
    return {
        get: function (url) {
            return $http.get(url);
        },
        post: function (url, data) {
            return $http.post(url, data);
        }
    };
});


myApp.controller('DayCtrl', ['$scope', '$http', function ($scope, $http) {
    $http.get('/Restaurants/rest/service/day').success(function (data, status) {
        $scope.dayWin = data;
    }).error(function (data, status) {
        console.error(data);
    })
}]);




myApp.controller('VoteCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.sendObject = []
    $scope.sendObject.restaurant = ""
    $scope.sendObject.employee = ""

    $http.get('/Restaurants/rest/service/restaurant/allActive').success(function (data, status) {
        $scope.restaurants = data
    }).error(function (data, status) {
        console.error(data);
    })
    $http.get('/Restaurants/rest/service/employee/findAllVote').success(function (data, status) {
        $scope.employees = data
    }).error(function (data, status) {
        console.error(data);
    })

    $scope.submit = function() {
        $http.post('/Restaurants/rest/service/vote', $scope.sendObject).success(function (data, status) {
            alert(data);
        }).error(function (data, status) {
            console.error(data);
        })

    };

}]);

myApp.controller('RankingCtrl', ['$scope', '$http', function ($scope, $http) {
    $http.get('/Restaurants/rest/service/ranking').success(function (data, status) {
        $scope.restaurants = data
    }).error(function (data, status) {
        console.error(data);
    })
}]);



myApp.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'views/main.html'
        })
        .when('/ranking', {
            templateUrl: 'views/ranking.html'
        })
        .when('/vote', {
            templateUrl: 'views/vote.html'
        })
        .otherwise({
            redirectTo: '/'
        });

}]);