package com.restaurant.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "voteOrder", query = "select r from Restaurant r where active is null or active = true order by votes DESC"),
        @NamedQuery(name = "dayOrder", query = "select r from Restaurant r where selectedDate is not null order by selectedDate DESC")
})
public class Restaurant implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String VOTE_ORDER = "voteOrder";
    public static final String DAY_ORDER = "dayOrder";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "restaurant_seq")
    @SequenceGenerator(name = "restaurant_seq", sequenceName = "restaurant_seq")
    private Integer id;

    private String name;
    private Boolean active;
    private Integer votes;

    @Temporal(TemporalType.DATE)
    @Column
    private Date selectedDate;


    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }


    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof Restaurant) {
            Restaurant restaurant = (Restaurant) obj;
            return restaurant.getId() == getId();
        }

        return false;
    }

}