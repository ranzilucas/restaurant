package com.restaurant.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by lucas on 17/07/14.
 */
@Entity
@NamedQuery(name = "findAllVote", query = "select e from Employee e where vote is null or vote = true")
public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String FIND_ALL_VOTE = "findAllVote";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private Boolean vote;

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof Employee) {
            Employee entity = (Employee) obj;
            return entity.getId() == getId();
        }

        return false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getVote() {
        return vote;
    }

    public void setVote(Boolean vote) {
        this.vote = vote;
    }
}
