package com.restaurant.scheduler;

/**
 * Created by lucas on 16/07/14.
 */

import com.restaurant.business.EmployeeBusiness;
import com.restaurant.business.RestaurantBusiness;
import com.restaurant.model.Employee;
import com.restaurant.model.Restaurant;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SchedulerJob implements Job {

    @Override
    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        Calendar calendar = Calendar.getInstance();
        RestaurantBusiness restaurantBusiness = new RestaurantBusiness();
        EmployeeBusiness employeeBusiness = new EmployeeBusiness();

        // verificar o maior vote
        Restaurant restaurantVote = restaurantBusiness.findVote().get(0);

        // adicionar o dia de hoje na tabela e setar com false campo de active
        if(restaurantVote != null) {
            restaurantVote.setSelectedDate(calendar.getTime());
            restaurantVote.setActive(false);
            restaurantBusiness.update(restaurantVote);
        }

        // executa uma vez por dia onde seta 0 nos votos
        for (Restaurant restaurant : restaurantBusiness.findAll()) {
            restaurant.setVotes(0);
            // se for domingo reset banco
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                restaurant.setActive(true);
            }
            restaurantBusiness.update(restaurant);
        }

        // percorre os funcionarios e habilita para votar
        for(Employee employee : employeeBusiness.findAll()){
            employee.setVote(true);
            employeeBusiness.update(employee);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        System.out.println("Quartz time " + simpleDateFormat.format(calendar.getTime()));
    }

}
