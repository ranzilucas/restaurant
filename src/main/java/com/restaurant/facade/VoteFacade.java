package com.restaurant.facade;

import com.restaurant.business.EmployeeBusiness;
import com.restaurant.business.RestaurantBusiness;
import com.restaurant.model.Employee;
import com.restaurant.model.Restaurant;

/**
 * Created by lucas on 17/07/14.
 */
public class VoteFacade {

    public String vote(String idRestaurant, String idEmployee) {
        EmployeeBusiness employeeBusiness = new EmployeeBusiness();
        RestaurantBusiness restaurantBusiness = new RestaurantBusiness();

        Employee employee = employeeBusiness.find(Integer.parseInt(idEmployee));
        Restaurant restaurant = restaurantBusiness.find(Integer.parseInt(idRestaurant));

        // se for false ele da erro
        if (!(restaurant.getActive() == null || restaurant.getActive())) {
            return "Erro ao votar Restaurante não permitido.";
        }
        // se for false ele da erro
        if ((!(employee.getVote() == null || employee.getVote()))) {
            return "Erro ao votar Funcionario selecionado ja votou hoje.";
        }

        employee.setVote(false);
        restaurant.setVotes(restaurant.getVotes() + 1);
        employeeBusiness.update(employee);
        restaurantBusiness.update(restaurant);

        return "Sucesso ao votar";
    }
}
