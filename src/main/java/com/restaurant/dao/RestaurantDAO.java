package com.restaurant.dao;

import com.restaurant.model.Restaurant;

import java.util.List;

/**
 * Created by lucas on 16/07/14.
 */
public class RestaurantDAO extends GenericDAO<Restaurant> {
    private static final long serialVersionUID = 1L;

    public RestaurantDAO() {
        super(Restaurant.class);
    }

    public List<Restaurant> voteOrder() {
        beginTransaction();
        List<Restaurant> restaurants = em
                .createNamedQuery(Restaurant.VOTE_ORDER, Restaurant.class)
                .getResultList();
        closeTransaction();
        return restaurants;
    }

    public List<Restaurant> dayOrder() {
        beginTransaction();
        List<Restaurant> restaurants = em
                .createNamedQuery(Restaurant.DAY_ORDER, Restaurant.class)
                .getResultList();
        closeConnection();
        return restaurants;
    }

}