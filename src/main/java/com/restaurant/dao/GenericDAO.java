package com.restaurant.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by lucas on 16/07/14.
 */
public abstract class GenericDAO<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    public GenericDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistencePU");
    protected EntityManager em;

    private Class<T> entityClass;

    protected void beginTransaction() {
        em = emf.createEntityManager();
        em.getTransaction().begin();
    }

    protected void closeTransaction() {
        em.close();
    }

    protected void commitAndCloseTransaction() {
        em.getTransaction().commit();
        em.close();
    }

    protected void closeConnection() {
        em.close();
    }

    protected void rollback() {
        em.getTransaction().rollback();
    }

    public void save(T entity) {
        beginTransaction();
        em.persist(entity);
        commitAndCloseTransaction();
    }

    public void update(T entity) {
        beginTransaction();
        em.merge(entity);
        commitAndCloseTransaction();
    }

    public T find(int entityID) {
        beginTransaction();
        T t = em.find(entityClass, entityID);
        closeConnection();
        return t;
    }

    public void remove(T entity) {
        beginTransaction();
        em.remove(entity);
        commitAndCloseTransaction();
    }

    public List<T> findAll() {
        beginTransaction();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        List<T> tList = em.createQuery(cq).getResultList();
        closeConnection();
        return tList;
    }

    public T findOneResult(String namedQuery, Map<String, Object> parameters) {
        T result = null;
        beginTransaction();
        try {
            Query query = em.createNamedQuery(namedQuery);

            // Method that will populate parameters if they are passed not null and empty
            if (parameters != null && !parameters.isEmpty()) {
                populateQueryParameters(query, parameters);
            }
            result = (T) query.getSingleResult();

        } catch (Exception e) {
            System.out.println("Error while running query: " + e.getMessage());
            e.printStackTrace();
        }
        closeTransaction();
        return result;
    }

    private void populateQueryParameters(Query query, Map<String, Object> parameters) {
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
    }
}