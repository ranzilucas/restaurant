package com.restaurant.dao;

import com.restaurant.model.Employee;

import java.util.List;

/**
 * Created by lucas on 16/07/14.
 */
public class EmployeeDAO extends GenericDAO<Employee> {
    private static final long serialVersionUID = 1L;

    public EmployeeDAO() {
        super(Employee.class);
    }

    public List<Employee> findAllVote() {
        beginTransaction();
        List<Employee> restaurants = em
                .createNamedQuery(Employee.FIND_ALL_VOTE, Employee.class)
                .getResultList();
        closeTransaction();
        return restaurants;
    }
}
