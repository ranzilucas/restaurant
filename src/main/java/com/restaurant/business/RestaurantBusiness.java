package com.restaurant.business;

import com.restaurant.dao.RestaurantDAO;
import com.restaurant.model.Restaurant;

import java.util.List;

/**
 * Created by lucas on 17/07/14.
 */
public class RestaurantBusiness {

    private RestaurantDAO restaurantDAO;

    public RestaurantBusiness() {
        restaurantDAO = new RestaurantDAO();
    }

    public void create(Restaurant restaurant) {
        restaurantDAO.save(restaurant);
    }

    public List<Restaurant> findAll() {
        return restaurantDAO.findAll();
    }

    public Restaurant find(int entityId) {
        return restaurantDAO.find(entityId);
    }

    public void update(Restaurant entity) {
        restaurantDAO.update(entity);
    }

    public List<Restaurant> findVote() {
        return restaurantDAO.voteOrder();
    }

    public List<Restaurant> findRanking() {
        return restaurantDAO.voteOrder();
    }

    public Restaurant findToday() {
        List<Restaurant> restaurants = restaurantDAO.dayOrder();
        if (restaurants != null) return restaurants.get(0);
        return null;
    }

}