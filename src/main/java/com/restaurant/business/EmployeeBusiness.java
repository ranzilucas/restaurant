package com.restaurant.business;

import com.restaurant.dao.EmployeeDAO;
import com.restaurant.model.Employee;

import java.util.List;

/**
 * Created by lucas on 17/07/14.
 */
public class EmployeeBusiness {

    private EmployeeDAO employeeDAO;

    public EmployeeBusiness() {
        employeeDAO = new EmployeeDAO();
    }

    public void create(Employee employee) {
        employeeDAO.save(employee);
    }

    public List<Employee> findAll() {
        return employeeDAO.findAll();
    }

    public List<Employee> findAllVote() {
        return employeeDAO.findAllVote();
    }

    public Employee find(int entityId) {
        return employeeDAO.find(entityId);
    }

    public void update(Employee entity) {
        employeeDAO.update(entity);
    }

}