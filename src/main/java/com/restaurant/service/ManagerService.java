package com.restaurant.service;

import com.google.gson.Gson;
import com.restaurant.business.EmployeeBusiness;
import com.restaurant.business.RestaurantBusiness;
import com.restaurant.facade.VoteFacade;
import com.restaurant.model.Employee;
import com.restaurant.model.Restaurant;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/service")
public class ManagerService {

    private RestaurantBusiness restaurantBusiness = new RestaurantBusiness();

    @GET
    @Path("restaurant/all")
    @Produces(MediaType.APPLICATION_JSON)
    public String allRestaurants() {
        return new Gson().toJson(restaurantBusiness.findAll());
    }

    @GET
    @Path("restaurant/allActive")
    @Produces(MediaType.APPLICATION_JSON)
    public String allActiveRestaurants() {
        return new Gson().toJson(restaurantBusiness.findVote());
    }

    @POST
    @Path("restaurant/create/{name}")
    public Response createRestaurant(@FormParam("name") String name) {

        Restaurant restaurant = new Restaurant();
        restaurant.setName(name);
        restaurantBusiness.create(restaurant);

        String result = "Create Restaurant, name =" + restaurant.getName() + " id =" + restaurant.getId();

        return Response.status(200).entity(result).build();
    }

    @GET
    @Path("day")
    @Produces(MediaType.APPLICATION_JSON)
    public String dayRestaurant() {
        return  new Gson().toJson(restaurantBusiness.findToday());
    }

    @GET
    @Path("ranking")
    @Produces(MediaType.APPLICATION_JSON)
    public String rankingRestaurant() {
        return new Gson().toJson(restaurantBusiness.findRanking());
    }

    @POST
    @Path("restaurant/update")
    public Response updateRestaurant(@FormParam("id") String id, @FormParam("name") String name) {

        Restaurant restaurant = restaurantBusiness.find(Integer.parseInt(id));
        restaurant.setName(name);
        restaurant.setVotes(0);
        restaurant.setActive(true);

        restaurantBusiness.update(restaurant);

        String result = "Update restaurant, name =" + restaurant.getName() + " id =" + restaurant.getId();

        return Response.status(200).entity(result).build();
    }

    @POST
    @Path("vote")
    public Response vote(@FormParam("restaurant") String restaurant, @FormParam("employee") String employee) {
        VoteFacade voteFacade = new VoteFacade();
        String response = voteFacade.vote(restaurant, employee);

        return Response.status(200).entity(response).build();
    }

    private EmployeeBusiness employeeBusiness = new EmployeeBusiness();

    @GET
    @Path("employee/all")
    @Produces(MediaType.APPLICATION_JSON)
    public String allEmployees() {
        return new Gson().toJson(employeeBusiness.findAll());
    }

    @GET
    @Path("employee/findAllVote")
    @Produces(MediaType.APPLICATION_JSON)
    public String findAllVoteEmployees() {
        return new Gson().toJson(employeeBusiness.findAllVote());
    }

    @POST
    @Path("employee/create")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createEmployee(@FormParam("name") String name) {

        Employee employee = new Employee();
        employee.setName(name);
        employee.setVote(true);

        employeeBusiness.create(employee);

        String result = "Create employee, name =" + employee.getName() + " id =" + employee.getId();

        return Response.status(200).entity(result).build();

    }

    @POST
    @Path("employee/update")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateEmployee(@FormParam("id") String id, @FormParam("name") String name) {

        Employee employee = employeeBusiness.find(Integer.parseInt(id));
        employee.setName(name);
        employee.setVote(true);

        employeeBusiness.update(employee);

        String result = "Update employee, name =" + employee.getName() + " id =" + employee.getId();

        return Response.status(200).entity(result).build();

    }


}
